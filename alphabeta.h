/*
	alphabeta.h	Alpha-Beta Pruning
	Copyright (c) 2000, 2001, 2003, 2004, 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef ALPHABETA_H
#define ALPHABETA_H

#include "board.h"
#include "iter.h"

struct search_info {
	byte_board_info		*board;
	empty_endgame_info	empties;
};

void	set_eval_random(int r);
int	get_eval_random();
void	set_midgame_depth(int depth);
void	eval_new_game();
int	eval_midgame(byte_board_info *board, int color, int *store_pos);
int	eval_endgame(byte_board_info *board, int color, int *store_pos);
int	eval_winlossdraw(byte_board_info *board, int color, int *store_pos, 
			 double komi = 0.0);

#endif /* ALPHABETA_H */
