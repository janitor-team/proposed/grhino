/*
	opening.h	Opening database
	Copyright (c) 2001, 2002 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef OPENING_H
#define OPENING_H

#include <deque>

#include "config.h"
#include "board.h"

struct opening_info {
	const char *	name;
	int		next_player;
	byte_board_type	board;
	size_t		num_move_sequence;
	const char *	move_sequence;
};

int get_num_opening();
const char *get_opening_name(int id);
int get_opening_id(const char *s);
int get_opening_player(int id);
int get_opening_player(const char *s);
int get_opening_num_move(int id);
int get_opening_num_move(const char *s);
const byte_board_type *get_opening_board(int id);
const byte_board_type *get_opening_board(const char *s);
void get_opening_move_sequence(int id, std::deque<int> &d);
void get_opening_move_sequence(const char *s, std::deque<int> &d);

#endif /* OPENING_H */
