/*
	test.cc
	Copyright (c) 2000, 2001, 2002, 2003, 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "boardio.h"
#include "hash.h"
#include "alphabeta.h"
#include "iter.h"

// Grab version number in VERSION variable
#undef VERSION
char *
#include "scripts/version"
;

const char *prog_name = "GRhino";
const char *prog_ver = VERSION;

byte_board_type board_test = {
	 0, 1,-1,-1,-1, 0, 0, 0, 
	 0, 1, 1, 1, 1, 1, 1, 0, 
	-1, 1,-1,-1,-1, 1,-1,-1, 
	-1, 1,-1,-1, 1,-1,-1,-1, 
	-1, 1,-1, 1,-1,-1,-1,-1, 
	 0, 1, 1,-1,-1,-1,-1,-1, 
	 0, 1,-1,-1,-1,-1,-1,-1, 
	 0, 0,-1,-1,-1,-1,-1,-1, 
};

// black to move
// h5, h6, p, g7, g6, h7, h8, g8
byte_board_type board_test2 = {
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1, 1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1, 1,-1, 1,-1,-1,-1,-1, 
	-1, 1, 1,-1,-1,-1,-1, 0, 
	-1, 1, 1,-1, 1, 1, 0, 0, 
	-1,-1, 1, 1, 1, 1, 0, 0, 
	 1,-1, 1, 1, 1, 1, 0, 0, 
};

// black to move
// h5, h6, p, g7, g6, h7, h8, g8
byte_board_type board_test3 = {
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	-1,-1,-1,-1,-1,-1,-1,-1, 
	 0,-1,-1, 1, 1, 0, 0, 0, 
	 0,-1, 0, 0, 0, 0, 0, 0, 
};

// FFO testsuite #40 (Turner vs Monnom, Bruxelles 1997)
// black to move
// a2 b1 c1 -- b6 c7 a7 b7 b8 d7 f8 c6 f7 g7, +38
byte_board_type board_ffo_40 = {
	-1, 0, 0,-1,-1,-1,-1, 1, 
	 0,-1,-1,-1,-1,-1,-1, 1, 
	-1,-1, 1, 1,-1,-1,-1, 1, 
	-1,-1, 1,-1,-1,-1, 1, 1, 
	-1,-1,-1,-1,-1,-1, 1, 1, 
	 0, 0, 0,-1,-1,-1,-1, 1, 
	 0, 0, 0, 0,-1, 0, 0, 1, 
	 0, 0, 0, 0, 0, 0, 0, 0, 
};

// FFO testsuite #44 (Shaman vs Tastet, WC 1995)
// white to move
// d2 g5 b8 (or b8 g5 d2) a3 b7 a8 a7 c8 g7 g8 h6 h5 g6 f1 d1 h1, -14
byte_board_type board_ffo_44 = {
	 0, 0,-1, 0, 1, 0,-1, 0, 
	 0, 0,-1, 0, 1,-1, 0,-1, 
	 0,-1,-1, 1, 1, 1,-1,-1, 
	-1,-1,-1,-1, 1, 1, 1,-1, 
	-1,-1,-1,-1, 1, 1, 0, 0, 
	 1, 1,-1,-1, 1,-1, 0, 0, 
	 0, 0, 1, 1, 1, 1, 0, 0, 
	 0, 0, 0, 1, 1, 1, 0, 0, 
};

// Simplified FFO testsuite #40 (Turner vs Monnom, Bruxelles 1997)
// black to move
// a7 b7 b8 d7 f8 c6 f7 g7, +38
byte_board_type board_ffo_40_simp = {
	-1,-1, 1, 1, 1, 1, 1, 1, 
	 1,-1, 1, 1, 1, 1, 1, 1, 
	-1,-1, 1,-1, 1,-1,-1, 1, 
	-1,-1, 1, 1,-1,-1, 1, 1, 
	-1,-1, 1,-1,-1,-1, 1, 1, 
	 0,-1, 0,-1,-1,-1,-1, 1, 
	 0, 0,-1, 0,-1, 0, 0, 1, 
	 0, 0, 0, 0, 0, 0, 0, 0, 
};

// Simplified FFO testsuite #44 (Shaman vs Tastet, WC 1995)
// white to move
// a7 c8 g7 g8 h6 h5 g6 f1 d1 h1, -14
byte_board_type board_ffo_44_simp = {
	 0, 0,-1, 0, 1, 0,-1, 0, 
	 0, 0,-1,-1,-1,-1, 0,-1, 
	 1, 1, 1, 1, 1, 1,-1,-1, 
	 1, 1,-1,-1, 1, 1, 1,-1, 
	 1,-1, 1, 1, 1, 1, 1, 0, 
	 1,-1, 1, 1, 1, 1, 0, 0, 
	 0, 1,-1, 1, 1, 1, 0, 0, 
	 1,-1, 0, 1, 1, 1, 0, 0, 
};

int main()
{
	trans_table_init();

/*	byte_board_info bb(&board_test2);
	int color = BLACK;*/

	byte_board_info bb(&board_test3);
	int color = BLACK;

/*	byte_board_info bb(&board_ffo_40);
	int color = BLACK;*/

/*	byte_board_info bb(&board_ffo_44);
	int color = WHITE;*/

/*	byte_board_info bb(&board_ffo_40_simp);
	int color = BLACK;*/

/*	byte_board_info bb(&board_ffo_44_simp);
	int color = WHITE;*/

	int p, x;
	std::cout << &bb;

	set_max_hash_move(57);

	for ( ; ; ) {
//		x = eval_winlossdraw(&bb, color, &p);
//free_hash_all_move();
		x = eval_endgame(&bb, color, &p);

		std::cout << " Score:" << x << " Move:";
		if (p != POS_PASS)
			print_pos(std::cout, p);
		else
			std::cout << "PASS";
		std::cout << ' ' << hash_hit << ':' << hash_miss << '\n';

		if (p != POS_PASS)
			bb.place_piece(color, p);

		std::cout << &bb;

		if (!bb.can_play())
			break;
		color = switch_color(color);
	}
	for (int i = 44; i < 60; ++i)
		get_hash_info(i);
}
