/*
	gutil.cc		GRhino GNOME Frontend Utilities
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include "gutil.h"

#include <gtk/gtk.h>

void error_message_box(const gtstream &bufstr)
{
	GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
						   GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
						   bufstr.str().c_str());
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

GdkPixbuf *scale_pixbuf_from_xpm_data(const char *xpm[], int width, int height)
{
	GdkPixbuf *p1, *p2;
	p1 = gdk_pixbuf_new_from_xpm_data(xpm);
	if (!p1)
		return p1;
	p2 = gdk_pixbuf_scale_simple(p1, width, height, GDK_INTERP_BILINEAR);
	g_object_unref(p1);
	return p2;
}
