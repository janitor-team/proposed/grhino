/*
	boardio.h	Game Board I/O
	Copyright (c) 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef BOARDIO_H
#define BOARDIO_H

#include "config.h"
#include "board.h"

#include <iostream>

std::ostream& operator<<(std::ostream &os, const byte_board_info *board);
void	print_indent(std::ostream &os, int indent = 0);
void	print_board(std::ostream &os, const byte_board_info *board, int indent = 0);
void	print_pos(std::ostream &os, int pos);

#endif /* BOARDIO_H */
