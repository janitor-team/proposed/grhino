/*
	hash.h		Transposition Hash Table
	Copyright (c) 2000, 2001, 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef HASH_H
#define HASH_H

#include "board.h"

#define NUM_TRANS_BOARD	100000
#define NUM_HASH	(1024*16)

struct	trans_board {
	byte_board_info	board;
	int		score;
	int		player;
	trans_board	*next;
};

extern unsigned int	hash_pos_table[64];

/* Hash value for color at pos */
inline int	get_hash_piece(int color, int pos)
{
	switch (color) {
		case BLACK:
			return hash_pos_table[pos] + (hash_pos_table[pos] << 16);
		case WHITE:
			return hash_pos_table[pos] << 16;
	}
	
	/* case EMPTY */
	return 0;
}

/* Hash value update when flipping piece at pos */
inline int	get_hash_flip(int pos)
{
	return hash_pos_table[pos];
}

/* For hashing performance monitoring */
extern int	hash_hit;
extern int	hash_miss;

void	trans_table_init();
int	get_hash(byte_board_info *board);
//int	get_hash(bit_byte_board_info *board);
trans_board *get_hash_board(byte_board_info *board, int player);
trans_board *get_temp_board(byte_board_info *board);
void	set_max_hash_move(int move);
inline void	store_hash_board(trans_board *trans, int score)
{
	if (trans) {
		trans->score = score;
	}
}


void	free_hash_move(int move);
void	free_hash_all_move();

void	get_hash_info(int move);

#endif /* HASH_H */
