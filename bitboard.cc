/*
	bitboard.cc	Game Board Operations for BitBoard
	Copyright (c) 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "bitboard.h"

bit_board_type bit_board_empty = {
	{ 0x00000000,
	  0x00000000 },
	{ 0x00000000,
	  0x00000000 }
};

bit_board_type bit_board_begin = {
	{ 0x00000008,
	  0x10000000 },
	{ 0x00000010,
	  0x08000000 }
};

		// No default initialization to speed up search
bit_board_info::bit_board_info()
{
}

// Fast bit counting essential for endgame search
int count_bits(int x)
{
	int count = bit_count_table[x & 0xFF];
	x >>= 8;
	count += bit_count_table[x & 0xFF];
	x >>= 8;
	count += bit_count_table[x & 0xFF];
	x >>= 8;
	count += bit_count_table[x & 0xFF];
	return count;
}

bit_board_info::bit_board_info(const bit_board_type *board_init)
{
	memcpy(&board, board_init, sizeof(board_type));
	int occupied_upper = board.black[0] | board.white[1];
	int occupied_lower = board.black[1] | board.white[1];
	move = count_bits(occupied_upper) + count_bits(occupied_lower) - 4;
	hash = get_hash(this);
}

bit_board_info& bit_board_info::operator=(const bit_board_type *board_init)
{
	memcpy(board, board_init, sizeof(bit_board_type));
	move = NUM_MOVE;
	board_iterator	iter;
	iter.init_pos();
	do {
		if (is_empty(iter.pos))
			move--;
	} while (iter.next());
	hash = get_hash(this);
	return *this;
}

bit_board_info& bit_board_info::operator=(const bit_board_info& src)
{
	memcpy(this, &src, sizeof(bit_board_info));
	return *this;
}

bool operator==(const bit_board_info &board1, const bit_board_info &board2)
{
	if (memcmp(&(board1.board), &(board2.board), sizeof(bit_board_type)))
		return false;
	return true;
}
