/*
	load.cc		Game File Loader
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef LOAD_H
#define LOAD_H

#include <string>

#include "log_proc.h"

struct game_library_log : public game_log {
	std::string	black_name;
	std::string	white_name;
	bool		random;

	game_library_log(int b, int w): game_log(b, w), random(false) {}

	void set_random() { random = true; }
	void set_black_name(const std::string& s, int idx, int len) {
		while (s[idx+len-1] == ' ')
			len--;
		black_name.assign(s, idx, len);
	}
	void set_white_name(const std::string& s, int idx, int len) {
		while (s[idx+len-1] == ' ')
			len--;
		white_name.assign(s, idx, len);
	}
};

void	load_game_list(const char *f);
void	load_game(const game_library_log &);

#endif /* LOAD_H */

