/*
	ggamewin.h	GRhino Gnome Frontend Game Library Window
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "load.h"

/*************************************************************************
	Configurations - GNOME specific
*************************************************************************/

extern int gamelib_width, gamelib_height, gamelib_x, gamelib_y;

/*************************************************************************
	Functions
*************************************************************************/

bool game_library_opened(const char *f);
void game_library(std::vector<game_library_log> *v, const char *f);
