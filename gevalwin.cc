/*
	gevalwin.cc	GRhino Gnome Frontend Evaluation Window
	Copyright (c) 2005, 2010 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
#define GNOME_DISABLE_DEPRECATED
#define GTK_DISABLE_DEPRECATED
*/

#include "config.h"

#include <gtk/gtk.h>
#include <gnome.h>

#include "board.h"
#include "pattern.h"
#include "parity.h"
#include "game.h"

/*************************************************************************
	Tools menu - Pattern evaluation
*************************************************************************/

int pattern_x, pattern_y;

GtkWidget *clist_eval;
GtkWidget *label_score;
GtkWidget *label_move_index;
GtkWidget *label_parity;

extern GnomeUIInfo menu_tools_info[];

void evaluate_position_fill_entry(pattern_t p, int index, int row, int col)
{
	static gchar s[5];	// Enough space for -127 .. 127 and string
				// termination
	int i = 0;

		// pattern_eval returns a signed char (-127 .. 127)
	int score = pattern_eval(view_board_ptr, p, index);
	if (score < 0) {
		s[i++] = '-';
		score = -score;
	}

	int score_left = score;
	if (score >= 100) {
		s[i++] = '1';
		score_left -= 100;
	}
	if (score >= 10) {
		s[i++] = score_left/10 + '0';
		score_left %= 10;
	}
	s[i++] = score_left + '0';
	s[i] = '\0';

	gtk_clist_set_text(GTK_CLIST(clist_eval), row, col, s);
}

void evaluate_position_fill()
{
	static gchar s[10];
	snprintf(s, 10, "%d", static_cast<int>(pattern_eval(view_board_ptr)));
	gtk_label_set_text(GTK_LABEL(label_score), s);
	snprintf(s, 10, "%d", static_cast<int>(to_move_index(view_board_ptr->get_num_move())));
	gtk_label_set_text(GTK_LABEL(label_move_index), s);
	snprintf(s, 10, "%d",
		 view_board_ptr->can_play() ? static_cast<int>(parity_eval(view_board_ptr, 
									   view_player))
					    : 0);
	gtk_label_set_text(GTK_LABEL(label_parity), s);

	evaluate_position_fill_entry(PATTERN_ROW1, 0, 0, 2);
	evaluate_position_fill_entry(PATTERN_ROW1, 1, 0, 4);
	evaluate_position_fill_entry(PATTERN_ROW1, 2, 1, 2);
	evaluate_position_fill_entry(PATTERN_ROW1, 3, 1, 4);
	evaluate_position_fill_entry(PATTERN_ROW2, 0, 2, 2);
	evaluate_position_fill_entry(PATTERN_ROW2, 1, 2, 4);
	evaluate_position_fill_entry(PATTERN_ROW2, 2, 3, 2);
	evaluate_position_fill_entry(PATTERN_ROW2, 3, 3, 4);
	evaluate_position_fill_entry(PATTERN_ROW3, 0, 4, 2);
	evaluate_position_fill_entry(PATTERN_ROW3, 1, 4, 4);
	evaluate_position_fill_entry(PATTERN_ROW3, 2, 5, 2);
	evaluate_position_fill_entry(PATTERN_ROW3, 3, 5, 4);
	evaluate_position_fill_entry(PATTERN_ROW4, 0, 6, 2);
	evaluate_position_fill_entry(PATTERN_ROW4, 1, 6, 4);
	evaluate_position_fill_entry(PATTERN_ROW4, 2, 7, 2);
	evaluate_position_fill_entry(PATTERN_ROW4, 3, 7, 4);

	evaluate_position_fill_entry(PATTERN_DIAG1, 0, 8, 2);
	evaluate_position_fill_entry(PATTERN_DIAG1, 1, 8, 4);
	evaluate_position_fill_entry(PATTERN_DIAG2, 0, 9, 2);
	evaluate_position_fill_entry(PATTERN_DIAG2, 1, 9, 4);
	evaluate_position_fill_entry(PATTERN_DIAG2, 2, 10, 2);
	evaluate_position_fill_entry(PATTERN_DIAG2, 3, 10, 4);
	evaluate_position_fill_entry(PATTERN_DIAG3, 0, 11, 2);
	evaluate_position_fill_entry(PATTERN_DIAG3, 1, 11, 4);
	evaluate_position_fill_entry(PATTERN_DIAG3, 2, 12, 2);
	evaluate_position_fill_entry(PATTERN_DIAG3, 3, 12, 4);
	evaluate_position_fill_entry(PATTERN_DIAG4, 0, 13, 2);
	evaluate_position_fill_entry(PATTERN_DIAG4, 1, 13, 4);
	evaluate_position_fill_entry(PATTERN_DIAG4, 2, 14, 2);
	evaluate_position_fill_entry(PATTERN_DIAG4, 3, 14, 4);
	evaluate_position_fill_entry(PATTERN_DIAG5, 0, 15, 2);
	evaluate_position_fill_entry(PATTERN_DIAG5, 1, 15, 4);
	evaluate_position_fill_entry(PATTERN_DIAG5, 2, 16, 2);
	evaluate_position_fill_entry(PATTERN_DIAG5, 3, 16, 4);

	evaluate_position_fill_entry(PATTERN_CORNER5X2, 0, 17, 2);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 1, 17, 4);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 2, 18, 2);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 3, 18, 4);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 4, 19, 2);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 5, 19, 4);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 6, 20, 2);
	evaluate_position_fill_entry(PATTERN_CORNER5X2, 7, 20, 4);
}

void evaluate_position_update(update_state_type /*u*/, void *)
{
	gtk_clist_freeze(GTK_CLIST(clist_eval));
	evaluate_position_fill();
	gtk_clist_thaw(GTK_CLIST(clist_eval));
}

gint evaluate_position_configure_event(GtkWidget *widget, 
				       GdkEventConfigure * /*e*/,
				       gpointer /*data*/)
{
	gtk_window_get_position(GTK_WINDOW(widget), &pattern_x, &pattern_y);
	return FALSE;		// Propagate signal to child so that it
				// can resize itself
}

void evaluate_position_closed(GtkWidget *widget, GtkWidget **data)
{
	update_move.remove(evaluate_position_update, 0);
	gtk_widget_destroy(widget);
	*data = NULL;
	show_pattern_evaluation = false;
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (menu_tools_info[2].widget),
				       show_pattern_evaluation);
}

void evaluate_position()
{
	static gchar empty[] = "";
	static gchar *row[5];

	static GtkWidget *evalbox;
	if (evalbox) {
		if (show_pattern_evaluation)
			gtk_window_present(GTK_WINDOW(evalbox));
		else
			evaluate_position_closed(evalbox, &evalbox);
		return;
	}

	if (!show_pattern_evaluation)
		return;

	row[0] = _("Pattern name");
	row[1] = _("Position");
	row[2] = _("Score");
	row[3] = _("Position");
	row[4] = _("Score");

	evalbox = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	if (pattern_x != -1 && pattern_y != -1)
		gtk_window_move(GTK_WINDOW(evalbox), pattern_x, pattern_y);
	gtk_window_set_title(GTK_WINDOW(evalbox), _("Pattern Evaluation"));
	gtk_container_set_border_width(GTK_CONTAINER(evalbox), 5);
	g_signal_connect(G_OBJECT(evalbox), "destroy",
			 G_CALLBACK(evaluate_position_closed), &evalbox);
	g_signal_connect(G_OBJECT(evalbox), "configure_event",
			 G_CALLBACK(evaluate_position_configure_event), NULL);

	GtkWidget *vbox = gtk_vbox_new(FALSE, 5);

	GtkWidget *hbox1 = gtk_hbox_new(TRUE, 5);
	GtkWidget *label1 = gtk_label_new(_("Total score"));
	gtk_box_pack_start(GTK_BOX(hbox1), label1, TRUE, TRUE, 2);
	label_score = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(hbox1), label_score, TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(vbox), hbox1, TRUE, TRUE, 2);

	GtkWidget *hbox2 = gtk_hbox_new(TRUE, 5);
	GtkWidget *label2 = gtk_label_new(_("Move index"));
	gtk_box_pack_start(GTK_BOX(hbox2), label2, TRUE, TRUE, 2);
	label_move_index = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(hbox2), label_move_index, TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, TRUE, TRUE, 2);

	GtkWidget *hbox3 = gtk_hbox_new(TRUE, 5);
	GtkWidget *label3 = gtk_label_new(_("Parity score"));
	gtk_box_pack_start(GTK_BOX(hbox3), label3, TRUE, TRUE, 2);
	label_parity = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(hbox3), label_parity, TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(vbox), hbox3, TRUE, TRUE, 2);

	clist_eval = gtk_clist_new_with_titles(5, row);
	gtk_box_pack_start(GTK_BOX(vbox), clist_eval, TRUE, TRUE, 2);
	gtk_container_add(GTK_CONTAINER(evalbox), vbox);

	gtk_clist_column_titles_passive(GTK_CLIST(clist_eval));
	row[2] = empty;
	row[4] = empty;
	row[0] = _("Row 1");
	row[1] = _("A1-H1");
	row[3] = _("A8-H8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("A1-A8");
	row[3] = _("H1-H8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Row 2");
	row[1] = _("A2-H2");
	row[3] = _("A7-H7");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("B1-B8");
	row[3] = _("G1-G8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Row 3");
	row[1] = _("A3-H3");
	row[3] = _("A6-H6");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("C1-C8");
	row[3] = _("F1-F8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Row 4");
	row[1] = _("A4-H4");
	row[3] = _("A5-H5");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("D1-D8");
	row[3] = _("E1-E8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);

	row[0] = _("Diag 1");
	row[1] = _("A1-H8");
	row[3] = _("H1-A8");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Diag 2");
	row[1] = _("A2-G8");
	row[3] = _("B1-H7");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("H2-B8");
	row[3] = _("G1-A7");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Diag 3");
	row[1] = _("A3-F8");
	row[3] = _("C1-H6");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("H3-C8");
	row[3] = _("F1-A6");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Diag 4");
	row[1] = _("A4-E8");
	row[3] = _("D1-H5");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("H4-D8");
	row[3] = _("E1-A5");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = _("Diag 5");
	row[1] = _("A5-D8");
	row[3] = _("E1-H4");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("H5-E8");
	row[3] = _("D1-A4");
	gtk_clist_append(GTK_CLIST(clist_eval), row);

	row[0] = _("Corner 5x2");
	row[1] = _("A1-E2");
	row[3] = _("H1-D2");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[0] = empty;
	row[1] = _("A8-E7");
	row[3] = _("H8-D7");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[1] = _("A1-B5");
	row[3] = _("A8-B4");
	gtk_clist_append(GTK_CLIST(clist_eval), row);
	row[1] = _("H1-G5");
	row[3] = _("H8-G4");
	gtk_clist_append(GTK_CLIST(clist_eval), row);

	evaluate_position_fill();
	update_move.add(evaluate_position_update, 0);

	gtk_widget_show_all(evalbox);
}

