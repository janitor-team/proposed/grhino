/*
	boardio.h	Game Board I/O
	Copyright (c) 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "boardio.h"
#include "iter.h"

std::ostream& operator<<(std::ostream &os, const byte_board_info *board)
{
	print_board(os, board);
	return os;
}

void	print_indent(std::ostream &os, int indent)
{
	for (int i = 0; i < indent; ++i)
		os << ' ';
}

void	print_board(std::ostream &os, const byte_board_info *board, int indent)
{
	board_iterator	iter;
	iter.init_pos();

	print_indent(os, indent);
	os << "  a b c d e f g h\n";

	int	col = 0, row = 1;
	do {
		if (col == 0) {
			print_indent(os, indent);
			os << row;
		}
		switch (board->board[iter.pos]) {
			case EMPTY:
				os << " .";
				break;
			case BLACK:
				os << " X";
				break;
			case WHITE:
				os << " O";
				break;
			default:
				os << " ?";
		}
		col++;
		if (col == 8) {
			os << '\n';
			col = 0;
			row++;
		}
	} while (iter.next());
}

void	print_pos(std::ostream &os, int pos)
{
	os << char(pos_to_x(pos)+'a') << char(pos_to_y(pos)+'1');
}
