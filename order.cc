/*
	order.cc	Move Ordering
	Copyright (c) 2000, 2001 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "order.h"

int	endgame_order[NUM_MOVE] = {
	A1, A8, H1, H8,			// Corners

	C3, C4, C5, C6,
	D3, E3, D6, E6,
	F3, F4, F5, F6,

	B3, B4, B5, B6,
	C2, D2, E2, F2,
	C7, D7, E7, F7,
	G3, G4, G5, G6,

	A3, A6, C1, F1,			// A-squares
	C8, F8, H3, H6,
	A4, A5, D1, E1,			// B-squares
	D8, E8, H4, H5,
	A2, A7, B1, G1,			// C-squares
	B8, G8, H2, H7,
	B2, B7, G2, G7			// X-squares
};

