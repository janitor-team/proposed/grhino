/*
	load.cc		Game File Loader
	Copyright (c) 2005, 2006 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include "load.h"
#include "game.h"
#include "gutil.h"
#include "ggamewin.h"

void	process_game(std::vector<game_library_log> &t, game_library_log &game)
{
	t.push_back(game);
}


void	load_game_list(const char *f)
{
	std::vector<game_library_log> *game_vec = new std::vector<game_library_log>;

	try {
		if (!game_library_opened(f))
			process_file<game_library_log>(f, *game_vec, false, false);
	}
	catch (std::exception &e) {
		gtstream bufstr;
		gtout(bufstr, _("Error: %$")) << e.what();
		error_message_box(bufstr);
		delete game_vec;
		return;
	}
	catch (...) {
		gtstream bufstr;
		gtout(bufstr, _("Error: problem reading file %$")) << f;
		error_message_box(bufstr);
		delete game_vec;
		return;
	}

	if (!game_vec->size()) {
		gtstream bufstr;
		gtout(bufstr, _("Error: file %$ does not contain game data")) << f;
		error_message_box(bufstr);
		delete game_vec;
		return;
	}
	else if (game_vec->size() == 1) {
					// Remove highlight from Game List
					// window
		update_game_list.update(UPDATE_ALL);
		load_game((*game_vec)[0]);
		delete game_vec;
		return;
	}
	else
		// game_library will handle memory release of game_vec
		game_library(game_vec, f);
}

void	cancel_edit_mode();			// In grhino.cc

void	load_game(const game_library_log &game)
{
	byte_board_info board = game.board;
				// Loop for all game moves
	for (int i = 0; i < game.num_move_queue; ++i) {
		int player = game.move_queue_color[i];
		int pos = game.move_queue[i];

				// Catch broken game record
		if (!board.can_play(player, pos)) {
			gtstream bufstr;
			gtout(bufstr, _("Error: game contains invalid move %$%$"))
				<< static_cast<char>('A' + pos_to_x(pos))
				<< static_cast<char>('1' + pos_to_y(pos));
			error_message_box(bufstr);
			return;
		}
		board.place_piece(player, pos);
	}

	cancel_input();
	cancel_edit_mode();

	if (game.random)
		cur_game_info.new_game_from_board(&(game.board), game.move_queue_color[0]);
	else
		cur_game_info.new_game_from_begin(0);
				// Loop for all game moves
				// FIXME: Deal with use_clock, time
	for (int i = 0; i < game.num_move_queue; ++i) {
		int pos = game.move_queue[i];
		cur_game_info.place_piece(pos, game.time_queue[i]);
	}

	if (game.timeout)
		cur_game_info.player_timeout();
	else if (game.resign)
		cur_game_info.player_resign();
	else if (cur_game_info.is_game_play())
		cur_game_info.game_end();

	*view_board_ptr = cur_game_info.board_history[0];
	view_player = cur_game_info.player_history[0];
	set_view_mode(VIEW_MODE_HISTORY);
	view_position = 0;
	draw_board();
	update_move.update(UPDATE_ALL);

				// Don't update update_game_list here
				// since it may be called from Game List
				// window itself.
}
